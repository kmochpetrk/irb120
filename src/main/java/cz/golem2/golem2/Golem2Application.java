package cz.golem2.golem2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Golem2Application {

	public static void main(String[] args) {
		SpringApplication.run(Golem2Application.class, args);
	}
}
