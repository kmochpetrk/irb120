package cz.golem2.golem2.model;

import java.util.ArrayList;
import java.util.List;

public class JointPositionsInteger {

    public JointPositionsInteger(List<Integer> jointPositions) {
        this.jointPositions = jointPositions;
    }

    public JointPositionsInteger() {
    }

    List<Integer> jointPositions = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JointPositionsInteger that = (JointPositionsInteger) o;

        return jointPositions != null ? jointPositions.equals(that.jointPositions) : that.jointPositions == null;
    }

    @Override
    public int hashCode() {
        return jointPositions != null ? jointPositions.hashCode() : 0;
    }

    public List<Integer> getjointPositions() {
        return jointPositions;
    }

    public void setjointPositions(List<Integer> jointPositions) {
        this.jointPositions = jointPositions;
    }
}
