package cz.golem2.golem2.model;

import java.util.ArrayList;
import java.util.List;

public class RobotLink {
    private LinkType linkType;
    private Vector3 length;
    private Vector3 startPos;
    private List<RobotJoint> joints = new ArrayList<>();

    public LinkType getLinkType() {
        return linkType;
    }

    public void setLinkType(LinkType linkType) {
        this.linkType = linkType;
    }

    public Vector3 getLength() {
        return length;
    }

    public void setLength(Vector3 length) {
        this.length = length;
    }

    public List<RobotJoint> getJoints() {
        return joints;
    }

    public void setJoints(List<RobotJoint> joints) {
        this.joints = joints;
    }

    public Vector3 getStartPos() {
        return startPos;
    }

    public void setStartPos(Vector3 startPos) {
        this.startPos = startPos;
    }
}
