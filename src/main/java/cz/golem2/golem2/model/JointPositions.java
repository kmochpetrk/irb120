package cz.golem2.golem2.model;

import java.util.ArrayList;
import java.util.List;

public class JointPositions {

    private Float axis1;
    private Float axis2;
    private Float axis3;
    private Float axis4;
    private Float axis5;
    private Float axis6;

    public Float getAxis1() {
        return axis1;
    }

    public void setAxis1(Float axis1) {
        this.axis1 = axis1;
    }

    public Float getAxis2() {
        return axis2;
    }

    public void setAxis2(Float axis2) {
        this.axis2 = axis2;
    }

    public Float getAxis3() {
        return axis3;
    }

    public void setAxis3(Float axis3) {
        this.axis3 = axis3;
    }

    public Float getAxis4() {
        return axis4;
    }

    public void setAxis4(Float axis4) {
        this.axis4 = axis4;
    }

    public Float getAxis5() {
        return axis5;
    }

    public void setAxis5(Float axis5) {
        this.axis5 = axis5;
    }

    public Float getAxis6() {
        return axis6;
    }

    public void setAxis6(Float axis6) {
        this.axis6 = axis6;
    }
}
