package cz.golem2.golem2.model;

import java.util.ArrayList;
import java.util.List;

public class KinematicsModel {
    private List<RobotLink> robotLinks = new ArrayList<>();

    public boolean add(RobotLink robotLink) {
        return robotLinks.add(robotLink);
    }

    public List<RobotLink> getRobotLinks() {
        return robotLinks;
    }
}
