package cz.golem2.golem2.model;

public enum LinkType {
    START,
    FINAL,
    MIDDLE;
}
