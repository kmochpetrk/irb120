package cz.golem2.golem2.model;

public class Vector3 implements Comparable<Vector3>{

    public Vector3(Integer x, Integer y, Integer z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    private Integer x;
    private Integer y;
    private Integer z;

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }


    public Integer getZ() {
        return z;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vector3 vector3 = (Vector3) o;

        if (!x.equals(vector3.x)) return false;
        if (!y.equals(vector3.y)) return false;
        return z.equals(vector3.z);
    }

    @Override
    public int hashCode() {
        int result = x.hashCode();
        result = 31 * result + y.hashCode();
        result = 31 * result + z.hashCode();
        return result;
    }

    @Override
    public int compareTo(Vector3 o) {
        return new Double((this.x*this.x + this.y*this.y + this.z*this.z) - (o.x*o.x + o.y*o.y + o.z*o.z)).intValue();
    }

    public Double getDistanceMetric(Vector3 other) {
        return new Double((this.x - other.x)*(this.x - other.x) + (this.y - other.y)*(this.y - other.y) +
                (this.z - other.z)*(this.z - other.z));
    }
}
