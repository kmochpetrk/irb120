package cz.golem2.golem2.model;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class JointPositionsToPoint3 {
    Map<JointPositionsInteger, Vector3> mapIntern = new HashMap<>();
    Map<Vector3, JointPositionsInteger> reverseMap = new HashMap<>();

    public Vector3 get(Object key) {
        return mapIntern.get(key);
    }

    public JointPositionsInteger getFromReverse(Vector3 vector3) {
        return reverseMap.get(vector3);
    }

    public Vector3 put(JointPositionsInteger key, Vector3 value) {
        reverseMap.put(value, key);
        return mapIntern.put(key, value);
    }

    public Vector3 getNearest(Vector3 inputPos) {
        double minimum = 1000000000d;
        Vector3 minVector = null;

        for (Map.Entry<Vector3, JointPositionsInteger> entry : this.reverseMap.entrySet()) {
            final Double distanceMetric = entry.getKey().getDistanceMetric(inputPos);
            if (minimum > distanceMetric) {
                minimum = distanceMetric;
                minVector = entry.getKey();
            }
        }
        return minVector;

//        final Map.Entry<Vector3, JointPositionsInteger> lower = this.reverseMap.ceilingEntry(inputPos);
//        final Map.Entry<Vector3, JointPositionsInteger> upper = this.reverseMap.floorEntry(inputPos);
//
//        return (lower.getKey().getDistanceMetric(inputPos).compareTo(lower.getKey().getDistanceMetric(inputPos)) > 0) ?
//                lower.getKey() : upper.getKey();
    }
}
