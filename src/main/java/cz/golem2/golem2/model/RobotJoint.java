package cz.golem2.golem2.model;

public class RobotJoint {
    private JointType jointType;
    private Float value;

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public JointType getJointType() {
        return jointType;
    }

    public void setJointType(JointType jointType) {
        this.jointType = jointType;
    }
}
