package cz.golem2.golem2.model;

public enum JointType {
    REVOLUTE,
    PRISMATIC;
}
