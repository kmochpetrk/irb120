package cz.golem2.golem2.model;

import Jama.Matrix;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Component
public class JointPositionsCreator {

    private JointPositionsToPoint3 jointPositionsToPoint3 = new JointPositionsToPoint3();


    @PostConstruct
    public void create() {
        System.out.println("Conf started!!!!!!!!!!!!!");
        // i = Math.atan2()
        for (int i = 0; i <= 180; i = i + 10) {
            System.out.println("Conf started!!!!!!!!!!!!! phase " + i);
            for (int j = -45; j <= 45; j = j + 10) {
                for (int k = -45; k <= 45; k = k + 10) {
                    for (int l = -45; l <= 45; l = l + 10) {
                        for (int m = -45; m <= 45; m = m + 10 ) {
                            for (int n = 0; n <= 0; n = n + 10) {
                                // calculate end position
                                final Vector3 endPos = getEndPos(i,j,k,l,m,n);
                                jointPositionsToPoint3.put(new JointPositionsInteger(Arrays.asList(i,j,k,l,m,n)), endPos);
                            }
                        }
                    }
                }
            }
        }
        System.out.println("Conf ended!!!!!!!!!!!");
    }

    private Vector3 getEndPos(int i, int j, int k, int l, int m, int n) {

        double mat01[][] = {{-1d, 0d, 0d, 0d},
                            {0d, 0d, 1d, 0d},
                            {0d, 1d, 0d, 0d},
                            {0d, 0d, 0d, 1d}};

        Matrix mat01J = new Matrix(mat01);


        double mat1[][] = {{Math.cos(((double)i/180)*Math.PI), Math.sin(((double)i/180)*Math.PI)*(-1), 0d, 0d},
                {Math.sin(((double)i/180)*Math.PI), Math.cos(((double)i/180)*Math.PI), 0d, 0d},
                {0d, 0d, 1d, 145d},
                {0d, 0d, 0d, 1d}};

        Matrix mat1J = new Matrix(mat1);

        double mat2[][] = {{Math.cos(((double)j/180)*Math.PI), 0d, Math.sin(((double)j/180)*Math.PI), 0d},
                {0d, 1d, 0d, 0d},
                {Math.sin(((double)j/180)*Math.PI)*(-1), 0d, Math.cos(((double)j/180)*Math.PI), 145d},
                {0d, 0d, 0d, 1d}};

        Matrix mat2J = new Matrix(mat2);

        double mat3[][] = {{Math.cos(((double)k/180)*Math.PI), 0d, Math.sin(((double)k/180)*Math.PI), 0d},
                {0d, 1d, 0d, 0d},
                {Math.sin(((double)k/180)*Math.PI)*(-1), 0d, Math.cos(((double)k/180)*Math.PI), 270d},
                {0d, 0d, 0d, 1d}};

        Matrix mat3J = new Matrix(mat3);

        double mat4[][] = {{1d, 0d, 0d, 134d},
                {0d, Math.cos(((double)l/180)*Math.PI), (-1)*Math.sin(((double)l/180)*Math.PI), 0d},
                {0d, Math.sin(((double)l/180)*Math.PI), Math.cos(((double)l/180)*Math.PI), 70d},
                {0d, 0d, 0d, 1d}};

        Matrix mat4J = new Matrix(mat4);

        double mat5[][] = {{Math.cos(((double)m/180)*Math.PI), 0d, Math.sin(((double)m/180)*Math.PI), 168d},
                {0d, 1d, 0d, 0d},
                {Math.sin(((double)m/180)*Math.PI)*(-1), 0d, Math.cos(((double)m/180)*Math.PI), 0d},
                {0d, 0d, 0d, 1d}};

        Matrix mat5J = new Matrix(mat5);

        double mat6[][] = {{1d, 0d, 0d, 72d},
                {0d, Math.cos(((double)n/180)*Math.PI), Math.sin(((double)n/180)*Math.PI)*(-1), 0d},
                {0d, Math.sin(((double)n/180)*Math.PI), Math.cos(((double)n/180)*Math.PI), 0d},
                {0d, 0d, 0d, 1d}};

        Matrix mat6J = new Matrix(mat6);


        double mat6E[][] = {{1d, 0d, 0d, 0d},
                            {0d, 1d, 0d, 0d},
                            {0d, 0d, 1d, 0d},
                            {0d, 0d, 0d, 1d}};

        Matrix mat6EJ = new Matrix(mat6E);

        Matrix result = mat01J.times(mat1J).times(mat2J).times(mat3J).times(mat4J).times(mat5J).times(mat6J).times(mat6EJ);

        return new Vector3(new Double(result.get(0, 3)).intValue(),new Double(result.get(1, 3)).intValue(),new Double(result.get(2, 3)).intValue());
    }

    public JointPositionsToPoint3 getJointPositionsToPoint3() {
        return jointPositionsToPoint3;
    }
}
