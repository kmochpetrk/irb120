package cz.golem2.golem2;

import cz.golem2.golem2.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RoboticController {

    @Autowired
    private JointPositionsCreator jointPositionsCreator;

    @RequestMapping(value = "/robot/calculateIk/{name}/{x}/{y}/{z}", method = RequestMethod.GET)
    public ResponseEntity<JointPositionsInteger> createNewRobot(@PathVariable(value = "name") String name,
                                                         @PathVariable(value = "x") Integer xPos,
                                                         @PathVariable(value = "y") Integer yPos,
                                                         @PathVariable(value = "z") Integer zPos) {

        JointPositionsToPoint3 jointPositionsToPoint3 = jointPositionsCreator.getJointPositionsToPoint3();

        Vector3 nearest = jointPositionsToPoint3.getNearest(new Vector3(xPos, yPos, zPos));

        final JointPositionsInteger fromReverse = jointPositionsToPoint3.getFromReverse(nearest);


        return new ResponseEntity<JointPositionsInteger>(fromReverse, HttpStatus.OK);

    }

}
